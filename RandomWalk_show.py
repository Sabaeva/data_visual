import matplotlib.pyplot as plt
from RandomWalk import RandomWalk

while True:
    # Построение случайного блуждания
    rw = RandomWalk(5000)
    rw.fill_walk()

    # Нанесение точек на диаграмму
    plt.style.use('classic')
    fig, ax = plt.subplots(figsize=(15, 9))
    point_numbers = range(rw.num_points)
    ax.scatter(rw.x, rw.y, c=point_numbers, cmap=plt.cm.Blues,
               edgecolors='none', s=15)
    ax.scatter(0, 0, c='black', edgecolors="none", s=50)
    ax.scatter(rw.x[-1], rw.y[-1], c='black', edgecolors='none', s=50)
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    plt.show()

    keep_running = input("Make another walk? (y/n): ")
    if keep_running == "n":
        break
