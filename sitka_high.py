import csv
from datetime import datetime
from matplotlib import pyplot as plt

filename = 'data/sitka_weather_2018_simple.txt'
# filename = 'data/death_valley_2018_simple.txt'
with open(filename) as f:
    reader = csv.reader(f)
    header_row = next(reader)

    # Считывание данных из файла
    dates, highs, lows, precipitations = [], [], [], []
    high_ind = header_row.index('TMAX')
    low_ind = header_row.index('TMIN')
    precip_ind = header_row.index('PRCP')
    for column in reader:
        current_date = datetime.strptime(column[2], "%Y-%m-%d")
        try:
            high = int(column[high_ind])
            low = int(column[low_ind])
            precipitation = float(column[precip_ind])
        except ValueError:
            print(f"Missimg data for {current_date}")
        else:
            dates.append(current_date)
            lows.append(low)
            highs.append(high)
            precipitations.append(precipitation)
    name = column [header_row.index('NAME')]
# Построение диаграммы для данных о температуре
plt.style.use('seaborn')
fig, ax = plt.subplots()
ax.plot(dates, highs, c='red', alpha=0.5)
ax.plot(dates, lows, c='blue', alpha=0.5)
plt.fill_between(dates, highs, lows, facecolor='blue', alpha=0.1)

# Оформление диаграммы для данных о температуре
plt.title(f"Daily high and low temperatures, 2018\n {name}",
          fontsize=20)
plt.xlabel('', fontsize=16)
fig.autofmt_xdate()
plt.ylabel('Temperatures (F)', fontsize=16)
plt.tick_params(axis='both', which='major', labelsize=16)

# Построение диаграммы для данных об осадках
plt.style.use('seaborn')
fig1,ax1 = plt.subplots()
ax1.plot(dates,precipitations, c='green')

# Оформление диаграммы для данных об осадках
fig1.autofmt_xdate()
plt.title(f"Daily precipitations, 2018\n {name}", fontsize=20)
plt.xlabel('', fontsize=16)
fig.autofmt_xdate()
plt.ylabel('Level of precipitations (mm)', fontsize=16)
plt.tick_params(axis='both', which='major', labelsize=16)

plt.show()

