from operator import itemgetter

import requests

url = 'https://hacker-news.firebaseio.com/v0/topstories.json'
r = requests.get(url)
print(f"Status code: {r.status_code}")

submissions_ids = r.json()
submissions_dicts = []
for submissions_id in submissions_ids[:5]:
    url = f"https://hacker-news.firebaseio.com/v0/item/{submissions_id}.json"
    r = requests.get(url)
    print(f"id: {submissions_id}\tstatus: {r.status_code}")
    response_dict = r.json()

    submissions_dict = {
        'title': response_dict['title'],
        'hn_link': f"https://news.ycombinator.com/item?id="
                   f"{submissions_id}",
        'comments': response_dict['descendants'],
    }
    submissions_dicts.append(submissions_dict)

submissions_dicts = sorted(submissions_dicts, key=itemgetter(
    'comments'), reverse=True)

for submissions_dict in submissions_dicts:
    print(f"\nTitle: {submissions_dict['title']}")
    print(f"Discussion link: {submissions_dict['hn_link']}")
    print(f"Comments: {submissions_dict['comments']}")