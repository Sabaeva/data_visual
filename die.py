from random import randint

class Die():
    """Класс, предтавляющий один кубик"""

    def __init__(self, num_sides=6):
        """Шестигранный кубик по умолчанию"""
        self.num_sides = num_sides

    def roll(self):
        """Возвращает случайное число от 1 до числа граней (по
        умолчанию=6)"""
        return randint(1, self.num_sides)
